
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
/** @jsx jsx */
import { jsx } from '@emotion/core';

import { sportListSelector } from './selector';
import { sPage, sWidget, sTable, sHeader } from './style';

import { TAppState } from '../../redux/reducer';
import { dataLoadTC } from '../../redux/hot-bets/thunk';
import BetsTable from '../../components/bets-table/bets-table';
import { setIndexView } from '../../redux/hot-bets/action';
import Tabs from '../../components/tabs/tabs';
import { HotIcon } from './../../attachment/icons';
import { TCellPath } from '../../components/bets-table/cell';


/**
 * Компонента Отрисовки виджета "Горячие ставки"
 * Тип: Контейнерная
 * Мемоизация: да
 *
 * @param {object[]} tableData данные для отрисовки таблицы
 * @param {func} dataLoadTC (THUNK) загрузка данных от сервера
 * @param {func} setIndexView (ACTION CREATOR) смена индекса страницы
 * @param {number} indexView индекс отображаемых данных(какой спорт отображен)
 *
 */
const HotBets = ({ tableData, dataLoadTC, setIndexView, indexView }: any) => {
    const tabsOnClick = (i: number) => {
        setIndexView(i)
    }

    const onClickEventCell = (path: TCellPath) => {
        const rowData = tableData.data[+path.row-1]
        const row = {
            team1: rowData.data.find((item: any) => item.name === 'team1').value,
            team2: rowData.data.find((item: any) => item.name === 'team2').value
        }
        const event = rowData.data[path.column]
        toastr.info('Инфо', `Выбран исход c ID ${event.id}: ${row.team1} – ${row.team2}, ${event.name}`)
    }

    return (
        <div css={sPage}>
            <button type="button" className="btn btn-light mb-3" onClick={() => dataLoadTC()}> Data loaded </button>
            <div css={sWidget}>
                <Tabs sHeader={sHeader} icon={HotIcon()} tabList={tableData && tableData.tabList} tabsOnClick={tabsOnClick} activeTab={indexView} />
                <BetsTable sTable={sTable} tableHead={tableData && tableData.tableHead} tableData={tableData && tableData.data} onClickCell={onClickEventCell} />
            </div>
        </div>
    );
}


const mapStateToProps = (state: TAppState) => ({
    tableData: sportListSelector(state.hotBets),
    indexView: state.hotBets.indexView
})

export default connect(mapStateToProps, { dataLoadTC, setIndexView })(HotBets)