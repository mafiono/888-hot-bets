/** @jsx jsx */
import { jsx } from '@emotion/core';
import Row, { TRowData } from './rows';
import { TCellPath } from './cell';

interface IProps {
    sTable?: any,
    tableData?: {id: number, data: TRowData[]}[],
    tableHead: TRowData[],
    onClickCell: (path: TCellPath) => void,
}

/**
 * Компонента вызова таблицы
 * Тип: Функциональная
 * Мемоизация: нет
 * @param {object[]} [tableHead] Данные заголовка 
 * @param {any} tableHead.value Значение передаваемое в ячейку заголовка
 * @param {string} tableHead.nameCSS наименование имени класса заголовка
 * @param {object[]} [tableData] Данные тела тоблицы
 * @param {number} tableData.id индификатор строки
 * @param {object[]} tableData.data Данные передаваеммые в ячейку
 * @param {any[]} tableData.value Значение передаваемое в ячейку
 * @param {string} tableData.nameCSS наименование имени класса 
 * @param {css} [sTable] стилизация таблицы
 * @param {func} [onClickCell] обработчик клика по ячейки
 * @returns <table>'TABLE'</table>
 */
const betsTable = ({ sTable, tableData, tableHead, onClickCell }: IProps) => {   
    return (
        <table css={sTable}>
            <tbody>
                {tableHead && <Row rowCSS='row-header' data={tableHead} indexRow={0}/>}
                {tableData && tableData.map((item, index) => <Row key={item.id} indexRow={index+1} data={item.data} onClickCell={onClickCell}/>)}
            </tbody>
        </table>
    )
}

export default betsTable