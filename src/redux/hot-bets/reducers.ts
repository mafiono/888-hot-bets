import { IInitialState, SET_SPORT_LIST, TSportAction, SET_INDEX_VIEW } from "./types";


const initialState: IInitialState = {
    sportList: undefined,
    indexView: 0
}

export function sportReducer(state = initialState, action: TSportAction): IInitialState {
    switch (action.type) {
        case SET_SPORT_LIST:
            if (JSON.stringify(state.sportList)===JSON.stringify(action.payload)){
                return state;
            }            
            return {...state, sportList: action.payload};

        case SET_INDEX_VIEW:
            return {...state, indexView: action.payload};

        default:
            return state;
    }
}