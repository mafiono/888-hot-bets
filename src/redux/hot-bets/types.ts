export const SET_SPORT_LIST = 'SET_SPORT_LIST'
export const SET_TAB_LIST = 'SET_TAB_LIST'
export const SET_INDEX_VIEW = 'SET_INDEX_VIEW'


export type TEvent = {id: number, price: number}
export type TTabList = {id: number, data: string}

export interface IGame {
    id: number,
    time: number,
    team1: string,
    team2: string,
    P1: TEvent,
    X: TEvent,
    P2: TEvent,
    countOptionalMarkets: number,
}

export interface ISportItem{
    id: number,
    name: string,
    gameList: IGame[]
    countGame: number
}

export interface IInitialState{
    sportList: undefined | ISportItem[]
    indexView: number
}

interface ISetSportList {
    type: typeof SET_SPORT_LIST,
    payload: ISportItem[]
}

interface ISetTabList {
    type: typeof SET_TAB_LIST,
    payload: TTabList[]
}

interface ISetSetIndexView {
    type: typeof SET_INDEX_VIEW,
    payload: number
}

export type TSportAction = ISetSportList | ISetTabList | ISetSetIndexView



