import { combineReducers } from "redux";
import { sportReducer } from "./hot-bets/reducers";
import { reducer as toastrReducer } from "react-redux-toastr";

const rootReducer = combineReducers({
    hotBets: sportReducer,    
    toastr: toastrReducer,
})

export type TAppState = ReturnType<typeof rootReducer>

export default rootReducer